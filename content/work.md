+++
title = "Work"

[extra]

[[extra.list]]
  name = "ByteDance"
  time = "2024.05 ~ 2024.08"
  location = 'Beijing, China'
  role = "Algorithm Engineer Intern"
  summary = 'Worked in the team responsible for the TikTok recommendation system that determines which products to recommend to users who are browsing short videos or conducting searches. Duties included optimizing Recall and Pre-Ranking stages, the first key stages in the recommendation flow.'
  details = [
    "Designed and implemented multi-phase experiments for allocating quotas among all recall methods in various scenarios, to expand the use of efficient methods and reduce the use of inefficient methods based on metrics such as click-through rate and order rate of each method, resulting in a 183k increase in daily gross merchandise volume.",
    'Optimized the strategy in the Pre-Ranking stage by grouping model-based and rule-based methods to eliminate bias issues between groups, resulting in a 36k increase in daily search page views.',
    "Improved the main Recall model by introducing a new Bilateral Softmax Loss function and adjusting the temperature parameter settings for positive and negative samples, resulting in an 18.94% increase in offline click-through rates, as well as a 2k increase in daily order."
  ]

[[extra.list]]
  name = "Fujian Cooby Information Technology Co., Ltd."
  time = "2021.06 ~ 2021.09"
  location = 'Fuzhou, China'
  role = "Software Engineer Intern"
  summary = ''
  details = [
    "Developed the home page API on PC and mobile for the official website of China Federation Of Overseas Chinese Entrepreneurs (CFOCE) while prioritizing scalability and high responsiveness.",
    "Implemented the ability to view reviewers' logs for Chinese Headline New Media (CHNM).",
  ]

[[extra.list]]
  name = "Fujian Santone Information Technology Co., Ltd."
  time = "2019.06 ~ 2019.09"
  location = 'Xiamen, China'
  role = "Signal Processing Engineer Intern"
  summary = ""
  details = [
    "Designed the GUI for the Digital Pre-Distortion (DPD) program by analyzing user needs and translating them into core requirements, building wireframes, creating interactive prototypes, and evaluating the UI design.",
    "Created and executed test plans for the DPD project along with designing and developing automated testing frameworks to validate functionality and ensure system robustness. ",
  ]

+++
