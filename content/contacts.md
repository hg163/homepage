+++
title = "Contact"
template = "contact-page.html"

[extra]
location = '510 S LaSalle St Apt 1120, Durham, NC 27705'
phone = '+ 1 (614) 967-0735'
links = [
  {icon = "github", name = "My projects on Github", link = "https://github.com/Huanli-Gong"},
  {icon = "gitlab", name = "My projects on Gitlab", link = "https://gitlab.oit.duke.edu/users/hg163/projects"},
  {icon = "linkedin", name = "Chat on Linkedin", link = "https://www.linkedin.com/in/huanli-gong-946903293/"},
  {icon = "resume", name = "View my resume", link = 'https://hg163.pages.oit.duke.edu/homepage/resume/Huanli Gong - Resume.pdf' },
  # {icon = "goodreads", icon_pack = "fab", name = "Discuss on", link = "https://www.goodreads.com/user/show/82099752-adrien"}
  # {icon = "telegram", icon_pack = "fab", name = "Telegram Me", link = "https://telegram.me/@Telegram"},
  ]
+++
