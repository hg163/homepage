+++
title = "出版物"

[extra]

[[extra.list]]
  authors = [ 'Huanli Gong', 'Liangming Pan', 'Hengchang Hu']
  year=2022
  name = "KHANQ: A Dataset for Generating Deep Questions in Education"
  booktitle =  "Proceedings of the 29th International Conference on Computational Linguistics"
  url = "https://aclanthology.org/2022.coling-1.518/"
  pdf=""

[[extra.list]]
  authors = ['Lingbo Mo', 'Huanli Gong', 'Sunit Singh', 'Chang-You Tai', 'Tianhao Zang', 'Tianshu Zhang', 'Huan Sun']
  year=2023
  name = "Taco 2.0: A Task-Oriented Dialogue System with Mixed Initiatives and Multi-Modal Interaction"
  booktitle = 'Alexa Prize TaskBot Challenge 2 Proceedings'
  url = "https://www.amazon.science/alexa-prize/proceedings/taco-2-0-a-task-oriented-dialogue-system-with-mixed-initiatives-and-multi-modal-interaction"
  pdf=""
+++
