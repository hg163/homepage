+++
title = "Project"

[extra]

[[extra.list]]
  name = "Design and Programming of Intelligent Assistant for the T-One Platform"
  time = "2024.06 ~ 2024.10"
  location = ''
  # role = "Mentored by Kailun Shan from Alibaba"
  role = ''
  summary = 'Developed an LLM-powered assistant for the Anolis community based on the open-source T-One platform. Built and indexed a local vector database from community documents, utilizing sparse vectors for keyword matching and dense vectors for semantic search, and implemented multi-turn conversational AI with RAG-based intelligent response generation.'
  details = [
  ]

[[extra.list]]
  name = "Social Reinforcement Learning Project"
  time = "2022.05 ~ 2022.08"
  location = ''
  # role = "Mentored by Rui Ding from Microsoft (China) Co"
  role = ''
  summary = 'Modeled news spreading on social networks and utilized Multi-Agent Deep Deterministic Policy Gradient (MADDPG) to solve dynamic processes on a graph, replicated with Offline Reinforcement Learning (MARWIL).'
  details = [
  ]
[[extra.list]]
  name = "Ruby on Rails Project: Web-based Presentations Evaluation Application"
  time = "2021.09 ~ 2021.12"
  location = ''
  role = ''
  summary = 'Implemented a web application would streamline the collection, collation, and analysis of audience evaluations of presentations. Instructors can manage users, presentations, and teams, and students can submit and view feedbacks. Extended features include authentication, admin dashboard, user alerts, output formatting, PostgreSQL with Heroku.'
  details = [
  ]

[[extra.list]]
  name = "Quandary language interpreter in java"
  time = "2022.01 ~ 2022.04"
  location = ''
  role = ''
  summary = 'Implemented a Quandary language interpreter an interpreter written in Java that has all language features including function calls, mutation, heap, and concurrency.'
  details = [
  ]

+++
