+++
title = "Research"

[extra]

[[extra.list]]
  name = "Amazon's Alexa Prize TaskBot Challenge 2"
  time = "2023.01 ~ 2023.09"
  location = ''
  role = "Member of The Ohio State University team"
  summary = 'Took the lead in Natural Language Understanding (NLU) subteam for ASR correction, intent classification, and question classification, and participated in the User Engagement subteam.'
  details = [
    "Developed and deployed a new ASR error correction module for TaskBot with a knowledge-based approach tailored for dialog scenarios, while evaluating its performance in comparison to various language models.",
    "Debugged and improved Intent Classifier, Question Classifier as well as other modules in the bot.",
    "Monitor user ratings and feedback and choose TaskBot-recommended tasks based on user interests."
  ]

[[extra.list]]
  name = "Automatic Speech Recognition (ASR) Error Correction"
  time = "2022.05 ~ 2022.12"
  location = ''
  role = "Research Assistant for Project Director (Prof. Huan Sun)"
  summary = "Proposed an approach to correct errors in ASR transcription and prepare it for Amazon's TaskBot Challenge 2."
  details = [
    "Implemented a pipeline to generate ASR errors by simulating noise, and used it to obtain noisy versions of datasets.",
    "Proposed a method to improve model performance in the pre-training process by not only training in terms of word meanings from context, but also in terms of pronunciation, decreasing word error rate by ~50%.",
    "Tested the performances of Language models for ASR error correction."
  ]

[[extra.list]]
  name = "Dataset for Generating Deep Questions in Education"
  time = "2021.08 ~ 2022.09"
  location = ''
  role = "Project Leader"
  summary = "Presented a dataset called KHANQ for models to generate deep questions on 29th International Conference on Computational Linguistics."
  details = [
    "Created a dataset by extracting, paraphrasing and annotating real questions and answers from students at Khan Academy.",
    "Used this dataset to train models including GPT, BART, and T5, and tested them with automatic (BLEU, METEOR, ROUGE-L) and human evaluations, analyzing the effects of different types of prompts, and developing conclusions about ways in which models are challenged as they generate human-like deep questions.",
  ]

+++
