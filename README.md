# Huanli Gong Homepage

## Demo
[Live demo](https://hg163.pages.oit.duke.edu/homepage)

![Theme screenshot](./screenshot.png)

![Video Demo](./Video.mov)

## Summary
This website is made with [Kodama](https://github.com/adfaure/kodama-theme) theme.

First lets introduce some technical details:
- It relies on [zola](https://www.getzola.org/).
- It has no javascript.
- The CSS is built with [tailwindcss](https://tailwindcss.com/).
    - The blog articles are themed with [@tailwindcss/typography](https://tailwindcss.com/docs/typography-plugin) theme.


## Website built with Zola

The best way to get started is to follow the official [zola tutorial](https://www.getzola.org/documentation/getting-started/overview/).

This theme can be installed as any other theme.

```
mkdir themes
cd themes & git clone https://gitlab.oit.duke.edu/hg163/homepage.git
```
and set in the config.toml the variable theme to kodama-theme.
### Getting started with Zola
Zola Commands:
- Build the website:

    ```bash
    zola build
    ```

- Run the website locally:

   ```bash
   zola serve
   ```
### Generate the CSS

[Tailwindcss](https://tailwindcss.com/) is a framework that parses your html files, and generate the minimal CSS required.
This theme depends on this framework.

The theme comes with the precompiled style files (`static/styles/styles.css`). However, if you wish to change the style, or modify the template htlm, you might need to recompile your styles.

The most simple way, is to follow the [installation page of tailwindcss](https://tailwindcss.com/docs/installation).

At the end, you should have tailwindcss installed, and I advise to use the following tailwind configuration:

```js
# tailwind.config.js
module.exports = {
  content: ["./templates/**/*.html", "./themes/**/*.html",  "./themes/**/*.html"],
  theme: {},
  variants: {},
  plugins: [
      require('@tailwindcss/typography'),
  ],
};
```

Create a file `styles/styles.css`, and use the following command to generate the final CSS file:

```
npx tailwindcss -i styles/styles.css -o static/styles/styles.css
```

The resulting file `static/styles/styles.css` is loaded in the html.

*Note that, for the moment the generation of the css is not automated. As a result, it is necessary to re-run this command when changes are made with the styling.*

### Configuration

This theme use some extra configuration, that can be set in the extra section of your `config.toml`.

```toml
# Title displayed in the index page
title = "Website title"

# Use this theme
theme = "kodama-theme"

[extra]

# Image of your avatar displayed on the landing page
avatar = "static/img/avatar.jpg"
# Image of the favicon
favicon = "static/img/avatar.jpg"

# Your email address showed in the contact section
email = "kodama[at]domain.com"

# If you don't want to show your contact information in the index
contact_in_index = true

# Additional menu items
# `Name` is what it is displayed, `path` is the url
menu_items = [
  { path = "#contacts", name = "Contact" },
]
```
## GitLab workflow to build and deploy site on push 
[Reference](https://www.getzola.org/documentation/deployment/gitlab-pages/)
1. Set the variable `base_url` in the `config.toml` to the url you want to build.
2. Create ```.gitlab-ci.yml``` in the root directory of the repository
```
stages:
  - deploy

default:
  image: debian:stable-slim

variables:
  # The runner will be able to pull your Zola theme when the strategy is
  # set to "recursive".
  GIT_SUBMODULE_STRATEGY: "recursive"

  # If you don't set a version here, your site will be built with the latest
  # version of Zola available in GitHub releases.
  # Use the semver (x.y.z) format to specify a version. For example: "0.17.2" or "0.18.0".
  ZOLA_VERSION:
    description: "The version of Zola used to build the site."
    value: ""

pages:
  stage: deploy
  script:
    - |
      apt-get update --assume-yes && apt-get install --assume-yes --no-install-recommends wget ca-certificates
      if [ $ZOLA_VERSION ]; then
        zola_url="https://github.com/getzola/zola/releases/download/v$ZOLA_VERSION/zola-v$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz"
        if ! wget --quiet --spider $zola_url; then
          echo "A Zola release with the specified version could not be found.";
          exit 1;
        fi
      else
        github_api_url="https://api.github.com/repos/getzola/zola/releases/latest"
        zola_url=$(
          wget --output-document - $github_api_url |
          grep "browser_download_url.*linux-gnu.tar.gz" |
          cut --delimiter : --fields 2,3 |
          tr --delete "\" "
        )
      fi
      wget $zola_url
      tar -xzf *.tar.gz
      ./zola build

  artifacts:
    paths:
      # This is the directory whose contents will be deployed to the GitLab Pages
      # server.
      # GitLab Pages expects a directory with this name by default.
      - public

  rules:
    # This rule makes it so that your website is published and updated only when
    # you push to the default branch of your repository (e.g. "master" or "main").
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

```
3. Get the URL at ```Deploy > Pages```: 
```https://hg163.pages.oit.duke.edu/homepage```
   
![GitLab Workflow](./Pages.png)

## Hosted on Netlify
1. Create ```netlify.toml``` in the root directory of the repository
```
[build]
publish = "/public"
command = "zola build"

[build.environment]
ZOLA_VERSION = "0.18.0"

[context.deploy-preview]
command = "zola build --base-url $DEPLOY_PRIME_URL"
```
2. Link [Netlify](https://www.netlify.com/) with GitLab, and deploy this project on Netlify
3. Here is the URL: ```https://huanligong.netlify.app/```

![Netlify](./Netlify.png) 