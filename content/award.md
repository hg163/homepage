+++
title = "Award"

[extra]

[[extra.list]]
  date='07-24-2024'
  name = "USPTO - Explainable AI for Patent Professionals"
  medal='Silver Medal'
  top=9
  url = "https://www.kaggle.com/certification/competitions/huanligong/uspto-explainable-ai"

[[extra.list]]
  date='11-17-2023'
  name = "Google - Fast or Slow? Predict AI Model Runtime"
  medal='Bronze Medal'
  top=11
  url = "https://www.kaggle.com/certification/competitions/huanligong/predict-ai-model-runtime"

[[extra.list]]
  date='06-27-2024'
  name = "AI Mathematical Olympiad - Progress Prize 1"
  medal='Bronze Medal'
  top=9
  url = "https://www.kaggle.com/certification/competitions/huanligong/ai-mathematical-olympiad-prize"

[[extra.list]]
  date='08-12-2024'
  name = "LMSYS - Chatbot Arena Human Preference Predictions"
  medal='Bronze Medal'
  top=6
  url = "https://www.kaggle.com/certification/competitions/huanligong/lmsys-chatbot-arena"

[[extra.list]]
  date='04-08-2024'
  name = "HMS - Harmful Brain Activity Classification"
  medal='Bronze Medal'
  top=6
  url = "https://www.kaggle.com/certification/competitions/huanligong/hms-harmful-brain-activity-classification"

[[extra.list]]
  date='01-22-2024'
  name = "LLM - Detect AI Generated Text"
  medal='Bronze Medal'
  top=8
  url = "https://www.kaggle.com/certification/competitions/huanligong/llm-detect-ai-generated-text"

[[extra.list]]
  date='2018-2019; 2019-2020'
  name = "Academic Merit Scholarship from Beijing Jiaotong University"

[[extra.list]]
  date='2024'
  name = "Love of Learning Award from The Honor Society of Phi Kappa Phi"
  url = "https://www.phikappaphi.org/grants-awards/love-of-learning"
+++

