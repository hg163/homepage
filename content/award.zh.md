+++
title = "奖项"

[extra]

[[extra.list]]
  date='2024-07-24'
  name = "USPTO - Explainable AI for Patent Professionals"
  medal='银牌'
  top=9
  url = "https://www.kaggle.com/certification/competitions/huanligong/uspto-explainable-ai"

[[extra.list]]
  date='2023-11-17'
  name = "Google - Fast or Slow? Predict AI Model Runtime"
  medal='铜牌'
  top=11
  url = "https://www.kaggle.com/certification/competitions/huanligong/predict-ai-model-runtime"

[[extra.list]]
  date='2024-06-27'
  name = "AI Mathematical Olympiad - Progress Prize 1"
  medal='铜牌'
  top=9
  url = "https://www.kaggle.com/certification/competitions/huanligong/ai-mathematical-olympiad-prize"

[[extra.list]]
  date='2024-08-12'
  name = "LMSYS - Chatbot Arena Human Preference Predictions"
  medal='铜牌'
  top=6
  url = "https://www.kaggle.com/certification/competitions/huanligong/lmsys-chatbot-arena"

[[extra.list]]
  date='2024-04-08'
  name = "HMS - Harmful Brain Activity Classification"
  medal='铜牌'
  top=6
  url = "https://www.kaggle.com/certification/competitions/huanligong/hms-harmful-brain-activity-classification"

[[extra.list]]
  date='2024-01-22'
  name = "LLM - Detect AI Generated Text"
  medal='铜牌'
  top=8
  url = "https://www.kaggle.com/certification/competitions/huanligong/llm-detect-ai-generated-text"

[[extra.list]]
  date='2018-2019; 2019-2020'
  name = "北京交通大学学习优秀奖学金"

[[extra.list]]
  date='2024'
  name = "Phi Kappa Phi荣誉协会 Love of Learning奖"
  url = "https://www.phikappaphi.org/grants-awards/love-of-learning"
+++

