+++
title = "科研"

[extra]

[[extra.list]]
  name = "Amazon's Alexa Prize TaskBot Challenge 2"
  time = "2023.01 ~ 2023.09"
  location = ''
  role = "俄亥俄州立大学队员"
  summary = '领导自然语言理解（NLU）分队，负责ASR纠正、意图分类、问题分类；并参与用户交互分队。'
  details = [
    "为TaskBot开发并部署了一个新的ASR错误校正模块，使用了针对对话场景优化的基于知识集的ASR错误纠正方法，并将其与各种语言模型进行了性能评估。",
    "调试并改进了意图分类器、问题分类器以及其他模块。",
    "监控用户评级和反馈，并根据用户兴趣选择TaskBot推荐的任务。"
  ]

[[extra.list]]
  name = "自动语音识别 (ASR) 错误纠正项目"
  time = "2022.05 ~ 2022.12"
  location = ''
  role = "实验室主任 (Prof. Huan Sun) 的研究助理"
  summary = "提出一种纠正ASR转录错误的方法，并将它运用于TaskBot Challenge 2。"
  details = [
    "实施了一个通过模拟噪声自动生成ASR错误的管道，并利用它获取噪声版本的数据集。",
    "提出了一种提高语言模型在ASR错误纠正任务上性能的方法，即预训练过程中模型除了从上下文中获取词义外，还被注入发音信息的编码，使词错误率降低 ~50%。",
    "测试用于 ASR 纠错的语言模型的性能。"
  ]

[[extra.list]]
  name = "教育领域深度问题的数据集"
  time = "2021.08 ~ 2022.09"
  location = ''
  role = "项目领导人"
  summary = "发布一篇关于为语言模型生成深度问题的数据集KHANQ的论文在NLP领域顶级会议（COLING）上。"
  details = [
    "召集并领导团队，通过提取、改写并标注可汗学院学生的真实问题和答案创建了一个数据集。",
    "使用该数据集训练了包括GPT、BART和T5在内的模型，并通过自动评估（BLEU、METEOR、ROUGE-L）和人工评估对模型进行测试，分析不同类型的prompt的影响，总结出模型在生成类人深度问题时所面临的挑战。",
  ]

+++
