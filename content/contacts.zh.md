+++
title = "联系方式"
template = "contact-page.html"

[extra]
links = [
  {icon = "github", name = "Github", link = "https://github.com/Huanli-Gong"},
  {icon = "gitlab", name = "Gitlab", link = "https://gitlab.oit.duke.edu/hg163"},
  {icon = "linkedin", name = "Linkedin", link = "https://www.linkedin.com/in/huanli-gong-946903293/"},
  {icon = "resume", name = "简历", link = 'https://hg163.pages.oit.duke.edu/homepage/resume/龚桓立-简历.pdf' }
  # {icon = "goodreads", icon_pack = "fab", name = "Discuss on", link = "https://www.goodreads.com/user/show/82099752-adrien"}
  # {icon = "telegram", icon_pack = "fab", name = "Telegram Me", link = "https://telegram.me/@Telegram"},
  ]
+++
